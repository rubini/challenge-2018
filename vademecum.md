# Challenge 2018

Aggiornato da rubi a Febbraio 2019, con considerazioni ex-post.

## Luogo e data

Inizio e fine a Caldirola, val Curone, 2-3 giugno 2018.


## Orari

Ritrovo ore 1500, fine ore 16 circa.


## Percorso

Sabato da caldirola al monte Giarolo e poi giù fino al prato
del pernotto (in tenda).

Pernotto qui: lat 44.7400, lon 9.1141

Mappa: [https://osm.org/go/xX1ui0j3?layers=C&m=](https://osm.org/go/xX1ui0j3?layers=C&m=)

Domenica si risale al Giarolo, si passa dall'Ebro, si torna indietro
200m per scendere al rifugio Orsi.


## Prove del sabato:

Vengono fatte partire prima le coppie lente e poi quelle
veloci, e facciamo un solo percorso visto che sono pochi.

0. equipaggiamento (all'attacco del sentiero, silvia)
1. pronto soccorso (sulla salita, valentina)
2. segnalazione (al giarolo, fabrizio)
3. natura (sulla discesa, fabio)
4. montaggio tenda (all'arrivo, rubi)
5. espressione (la sera, cincia)

Ogni persona responsabile della prova deve procurare materiale e
se riesce un compagno di merende.

Ogni prova prende da 0 a 5 punti, se non viene fatta sul percorso ma
la recuperano dopo possono prendere massimo 2.

La prova 5 viene data come compito a casa: ogni coppia deve preparare
4-5 minuti di intrattenimento per il bivacco serale, che faremo
poi nel prato dove si piantano le tende.

## Prove della domenica

6. cronoscalata (matilde alla partenza, carlo sull'ebro)
7. nodi (al giarolo, rubi)
8. topografia (all'ebro, carlo)
9. valutazione tempo di cammino (prima del rifugio, fabrizio)

La 6 è il tempo dal via (ore 7, vedi sotto) fino alla cima del monte Ebro.

Per la 7 sono stati dati due problemi da risolvere con corde fornite da noi.
Ognuno lo fa coi nodi che crede, si valuta il risultato
non il nodo. La prova 7 viene preparata sulla cima del Giarolo, se
fossero state tante coppie avremmo fatto due istanze della prova per non
fare coda, ma poi è servito.

La 8 si è fatta in cima all'Ebro, il punto più alto. Anche
qui problemi da risolvere (orientare la cartina per sapere dove si è,
cose così). Si veda la sezione cartine per il materiale.

Nella 9 si chiede di effettuare il percorso dall'Ebro al rifugio Orsi
in un tempo imposto. È una prova di regolarità (e stima del tempo).
Abbiamo deciso 52 minuti, che è un tempo da passeggiata, non di corsa.

## Partecipanti

Per ogni gruppo ragazzi e magliette sono stati:

* pv1: 2f 1m  (3M 3L 2XL)
* pv4: 1f 1m (4M)
* voghera: 1f (1M)
* mede: 2f 2m (2M 2L)
* garlasco: 1m (coppia con mortara) (2M 1L)
* vigevano: 2f 5m (2M 5L 2XL)
* mortara: 2f 1m (coppia con garlasco) (3M 2L)

I capi avrebbero dovuto essere:

* rubi
* matilde
* carlo (arriva sabato sera)
* fabio
* silvia (solo sabato pome)
* ludovica
* cincia (poi assente)
* fabrizio
* monica
* valentina
* michele


## Orari sabato

Alle 15 cerchio, chiusura cellulari (come stimolo non come
costrizione), lancio, [preghiera](https://gitlab.com/rubini/challenge-2018/raw/master/preghiera.jpeg) fornita da Fabio (niente don), prima partenza ore 1530.
Poi una partenza ogni 5 minuti.
Pensavamo al Giarolo entro le 20, ma poi mi pare abbiamo accorciato.
Abbiamo detto che avrebbero dovuto essere al posto del pernotto entro
le 20, mi pare (30-40m di discesa, più la prova natura). O forse eano le 21.

Nota: anche il capo che parte per ultimo, non dovendo fare le prove,
riesce ad arrivare al posto del pernottamento prima dei
ragazzi. Quindi la tenda per la prova montaggio o una delle due la
può anche portare il capo evento senza creare problemi.


## Orari domenica

Ore 630 primo fischio: si possono alzare, lavare far colazione, non
possono toccare la tenda (lo zaino si: difficile fornire una
soglia tra mettere via lo spazzolino e mettere via il sacco a pelo).

Ore 700 secondo fischio: possono smontare la tenda e partire.
Consegna buste (matilde).  Verifica (vedi sotto) e via.

Ci si aspetta che arrivino al rifugio Orsi tra le 12 e le 1230.  Tra
1230 e 1500 abbiamo pranzo, messa, premiazioni, chiusura.  Si torna a
caldirola tutti insieme per il sentiero, arrivo previsto ore 16.


## Percorso e gestione dispersi

Al lancio il sabato la prova equipaggiamento è dove parte
il sentiero, per evitare il tempo perso a cercare l'attacco.

Poi non avrebbero dovuto perdersi, quindi alla fine non avevamo
il servizio scopa.  In realtà hanno sbagliato strada in molti, dove
sulla salita si stacca un sentiero secondario sulla sinistra (mentre il
principale rimane dove si sente il torrentello).

Maurizio ha procurato le radio della regione, sono state utili.

Domenica un capo all'uscita dell'accampamento verifica con loro che
stiano salendo al Giarolo: se partono giusti non si perdono.  Sul
Giarolo c'è la prova nodi, che verifica che prendano la direzione
giusta sulla cresta.

Il resto del sentiero è in cresta: c'è un solo punto in cui si può
sbagliare perché il sentiero principale scende; prepariamo un cartello
da mettere sulla strada sbagliata, che verrà tolto da rubi+matilde
che arrivano per ultimi.  Dall'Ebro a scendere non si può sbagliare.
Il cartello non sembra sia servito, e alla fine non lo abbiamo trovato.

## Cibo

Cena al sacco, si arrangiano loro (probabilmente una volta arrivati
al campo).

Colazione fornita (acqua, bustine, biscotti).  Devono
portare un fornellino.

Pranzo: salamelle alla brace (cioè panino con salamella), usando il
braciere apposito del rifugio Orsi. Preparano fabio e valentina.

Biscotti e bustine ce li dividiamo noi capi. Chi zero chi tre pacchi
di biscotti, ce la facciamo.  Per le salamelle e il pane ci procura
il rifugio il materiale, bisogna dare il numero preciso una settimana
prima (338/4964613, signor mauro).


## Premi

C'è un tondo di legno che ci si passa da quando esiste l'agesci.

Altri premi (forse da sfoltire perché non siamo più con zona ProMiSE).:

 *  primo: opinel
 *  secondo: torcia frontale
 *  terzo: posate pieghevoli

Tutto in triplo perché potrebbe vincere la terna, quello che
resta è materiale di zona per l'anno prossimo. Al momento li ha rubi,
chiedeteglieli (349-2689041).

## Magliette

È usanza dare una maglietta ad ogni ragazzo. La scritta è sempre
la stessa cambia il colore.  Quest'anno arancione. Le abbiamo
sempre fatte da Fox Sport di Vigevano (0381-329.809,  foxvigevano@libero.it).
Costano 4.30 EUR l'una e abbiamo dato i numeri lunedi 14. Alla fine
mancavano due magliette (una l'abbiamo data al don Ale).


## Preventivo

Spese fisse: 160 [80 (premi) + 50 (rimborso rifugio) + 30 (benzine scopa)]
Spese per ogni persona: 8 [4.3 (maglietta) + 3.7 (cibo)].

Forse 10 euro non bastano, abbiamo deciso di provare con 12.5.
Forse 10 per i capi, da vedere.  Abbiamo valutato una compensazione
viaggi ma è troppo complicato. Per le benzine ogni gruppo fa come suo
solito.

## Consuntivo

### Entrate (375)
   30  avanzo uscita di zona rs
  225  18 ragazzi x 12.5
   20  garlasco (fabrizio, monica)
   20  pv4 (maurizio, fabio)
   20  vigevano (valentina, michele)
   10  mortara (carlo)
   10  maglietta ludo + don ale
   10  2 magliette silvia
   10  2 magliette vigevano
   20  pv1 (rubi + 2 magliette)

### Uscite (374)

  150  pranzo (ci hanno fatto 5 x 30)
   60  premi (59.91, scontrino di fabrizio)
  146  magliette (146.20, scontrino di valentina)
    6  colazione (6.66, scontrino rubi)
   12  cartoleria (11.76 stampe a colori, fotocopie, buste, scontrini rubi)

### Note

Non abbiamo rimborsato la benzina all'amico che ha riportato a casa la
capa malata domenica mattina. Se avessimo avuto le spese previste
(servizio scopa e premi più cari) e non avessimo avuto i 30 dalla
gara di canto saremmo andati in negativo. E poi ciascuno ci ha messo
qualcosa tra benzine cibo in più eccetera.

Avevo chiesto 32 panini con salamella, e così erano e ci hanno
chiesto 150.  Alla fine eravamo in 18+9 = 27 (il cato ha saltato). Il
rifugio ha fornito anche le mele. Avevamo preventivato 50 come
contributo spese al rifugio e 3.7 a persona per il pranzo. Avrebbe
fatto 149.9 (contando 27) ma siccome avremmo dovuto essere di più ho
offerto 170 al rifugio spiegando che era il nostro preventivo, ma il
signor mauro ha detto che sicuramente li usiamo bene per i ragazzi e
ha voluto solo i 150.


## Cose da fare (sono state fatte, restano qui a futura memoria)

* Preparare lettera di invito (ludo)
* Recuperare entrambe le cartine ad alta risoluzione (rubi o fabio, da edo)
* Preparare cartine a piccola scala per carlo (rubi)
* Scrivere lettera di missione da consegnare alla partenza (ludo)
* Preparare materiale di cancelleria (buste ecc) (rubi)
* Recuperare il premio itinerante (vale)
* Preparare altri premi (fabrizio)
* Recuperare magliette (valentina)
* Dare il numero di panini e salamelle al rifugio (rubi)
* Preparare la preghiera iniziale e trovare un celebrante (fabio)
* Predisporre un piano B in caso di mareggiata (maurizio)


## Cosa abbiamo sbagliato

Il capo evento deve cercare di imprimersi in testa tutte le
coppie/terne che partono e ricordarsi di far scegliere il nome prima
della partenza: è importante sapere chi c'è e chi manca se qualcuno
sbaglia strada. Mettendo una freccia all'unico bivio rischioso forse
si risolve (o mettendo la prova di pronto soccorso in quel punto).

## Contatti

È importante che i capi abbiano i contatti degli altri capi, non siamo
stati bravi in questo. 
Prego tutti i partecipanti di fornirmi i propri recapiti, specificando
possibilmente il mezzo di comunicazione preferenziale.

In ogni casi i miei contatti sono i seguenti:

* posta: `rubini@gnudd.com` (preferenziale)
* voce/sms: 349-2689041 (a qualunque ora)
* w.app: 328-2469733

## Informazioni aggiuntive sulle prove (FIXME: non è completo)

0. equipaggiamento: niente nuove buone nuove.

1. pronto soccorso: [materiale fornito da Valentina](https://gitlab.com/rubini/challenge-2018/raw/master/prova-ps.docx) (docx)

2. segnalazione: non abbiamo niente

3. natura: [materiale fornito da Fabio](https://gitlab.com/rubini/challenge-2018/raw/master/prova-natura.pages) (non so che formato sia, roba mac credo).

4. montaggio tenda: abbiamo fatto montare due tende uguali, così due
coppie lavoravano insieme. Siccome gli igloo stanno su comunque, e
siccome gli schifiltor decathlon non sono mai tesi, abbiamo contato
solo il tempo e non la qualità.

5. espressione: al momento non trovo i file forniti da Eleonora

6. conoscalata: nessun materiale

7. nodi: la prova è stata tendere una corda tra un picchetto in terra
e la ringhiera del monumento, e collegare due corde di dimensione
diversa.  Ognuno col nodo che vuole. Nel secondo caso è apprezzato il
nodo di bandiera.

8. topografia: FIXME devo recuperare info

9. valutazione tempo di cammino: nessun materiale

## Altri allegati

Lettera di invito:
[originale](https://gitlab.com/rubini/challenge-2018/raw/master/invito.docx)
fornito da Ludovica,
[finale](https://gitlab.com/rubini/challenge-2018/raw/master/invito2.docx)
stampata e consegnata in busta.

Cartine: FIXME devo recuperare tutto quanto.